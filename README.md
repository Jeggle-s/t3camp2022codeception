# T3Camp2022Codeception

A boilertemplate for a testing setup with TYPO3 and Codeception.
In the directory `tests/codeception` you can find a basic configuration and folder structure for codeception. 
You can also take a look in the `docker-compose.selenium.yaml` inside of the `.ddev` directory. This enables you to test inside your ddev-Container. The `test` and `test_multi` commands of ddev help you to run the tests (`.ddev/commands/web`)

## Running commands

### Test

1. Running an entire test:
`ddev test /path/to/your/ExampleCest.php`

2. Runnung a method of a test:
`ddev test /path/to/your/ExampleCest.php::testStartSlider`

## Directories
`.ddev` contains configurations for your local ddev environment e.g. Docker-Configuration or commands.
`htdocs` already contains all required packages, so this repo is ready to run out of the box.
`tests/htdocs` contains the codeception configuration both for your ddev environment and GitLab-CI.

Relevent directories under `tests/htdocs/tests` are `_support` where you can find helpder classes and [page objects](https://codeception.com/docs/06-ReusingTestCode). The directory `Acceptance` shows you an example testclass.

The project is ready to use if you have ddev and Docker running on your local machine. The example test just tests the navigationbar on typo3.org.

If there are any Questions just create an [issue](https://gitlab.com/Jeggle-s/t3camp2022codeception/-/issues)
