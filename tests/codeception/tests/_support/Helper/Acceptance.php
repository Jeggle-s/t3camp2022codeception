<?php

namespace Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I

class Acceptance extends \Codeception\Module
{
    // HOOK: after suite
    public function _afterSuite()
    {
    }

    // HOOK: before suite
    public function _beforeSuite($settings = array())
    {
    }

    // HOOK: on fail
    public function _failed(\Codeception\TestInterface $test, $fail)
    {
    }
}
