<?php


namespace Page;

/*
 * Innerhalb eines PageObjects werden alle Daten (Selektoren etc.) hinterlegt, welche für die entsprechende Seite
 * relevant sind. Pro Seite muss ein neues PageObject angelegt werden.
 * Die BasePage beinhaltet alle Daten, die für alle Seiten gültig sind (Navigation, Cookies etc...)
 * */

/**
 * Class BasePage
 *
 * @author Autor Name <eliseo.malo@xima.de>
 */
class BasePage
{
    /*
     * Prefixe URL, DATA, SELECTOR und JS dienen zu besseren Unterscheidung im Test
     * */

    const URL_BASE_URL = '/';
    const DATA_SEARCH_TERM = 'Dresden';

    // CSS und Xpath Selektoren sind erlaubt
    const SELECTOR_MAIN_NAVIGATION = '#mainNav';
    const JS_EMPTY_LOCAL_STORAGE = 'localStorage.clear();';
}
