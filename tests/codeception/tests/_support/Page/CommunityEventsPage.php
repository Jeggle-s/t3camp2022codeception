<?php


namespace Page;

/*
 * PageObject einer spezifischen Seite
 * */

/**
 * Class TemplatePage
 *
 * @author Autor Name <eliseo.malo@xima.de>
 */
class CommunityEventsPage
{
    const URL_DEFAULT_URL = '/community/events';
    const SELECTOR_EVENT_LIST = '#accordion';
}
