<?php

/*
 * Import des PageObjects der Seite auf welcher der Test ausgeführt wird
 * */

namespace codeception\tests\Acceptance;

use AcceptanceTester;
use Page\CommunityEventsPage;
use Page\BasePage;

/*
 * Import des Basis PageObjects der Website
 * */

/**
 * Class ExampleCest
 *
 * @author Autor Name <eliseo.malo@xima.de>
 */
class ExampleCest
{

    /*
     * _before Hook.
     * Hier muss geprüft werden, ob man sich auf der Seite befindet, von welcher der Test ausgehen soll.
     * */
    public function _before(AcceptanceTester $I)
    {
        // Die URL wird im TemplatePage PageObject definiert
        $I->amOnPage(CommunityEventsPage::URL_DEFAULT_URL);
    }

    /*
     * Jedes Szenario muss durch eine Funktion abgebildet werden.
     * Es dürfen nicht mehrere Szenarien in einer Funktion vereint werden.
     * */
    public function startPageNavigation(AcceptanceTester $I)
    {
        // Zu Beginn eines jeden Szenarios wird erklärt was erreicht werden soll
        $I->wantTo('Test if the navigation is visible');
        $I->seeElement(BasePage::SELECTOR_MAIN_NAVIGATION);
        $I->makeScreenshot('my_screenshot');

    }
}
